% Down phase
function[down_enabled, down_output_sum, down_transposed_convolution_sum, down_inv_pooled, down_transposed_convolution, down_output] = down_phase(up_input, up_output, current_input, pooled_indexes, pool_size, filter, bias, features, last_down_enabled)
        layers = size(filter, 2);
	% from the last layer to the first layer do...
        for layer_index = layers:-1:1
                % initialize the output to zeros
                down_output_sum{layer_index} = zeros(1, size(up_input{layer_index, 1}, 2));
                % 
                down_transposed_convolution_sum{layer_index} = zeros(1, size(up_input{layer_index, 1}, 2));
                %
		down_enabled{layer_index} = ones(1, size(up_input{layer_index, 1}, 2));
		% from the first feature to the last one do...
                for feature_index = 1:features{layer_index}
                        % we need to get the original feature as during the pooling 
                        % the residue of the division might have skipped units
                        original_size = size(up_output{layer_index, feature_index}, 2);
                        % upsample the donwnsampled layer 
                        down_inv_pooled{layer_index, feature_index} = inv_max_pooling(
                                                                                current_input,
                                                                                pooled_indexes{layer_index, feature_index},
                                                                                pool_size{layer_index},
                                                                                original_size);
                        down_enabled_inv_max_pooled{layer_index} = inv_max_pooling(
                                                                                last_down_enabled,
                                                                                pooled_indexes{layer_index, feature_index},
                                                                                pool_size{layer_index},
                                                                                original_size);
			% make the transposed convolution of the upsampled layer
                        down_transposed_convolution{layer_index, feature_index} = transposed_convolution(
                                                                                                filter{layer_index, feature_index},
                                                                                                bias{layer_index, feature_index},
                                                                                                down_inv_pooled{layer_index, feature_index});
			
                        down_enabled{layer_index} = transposed_convolution_enabled(
                                                                                                size(filter{layer_index, feature_index}, 2),
                                                                                                down_enabled_inv_max_pooled{layer_index});

			filter_size = size(filter{layer_index, feature_index}, 2);
			down_enabled{layer_index}(1: filter_size - 1) = 0; 
			down_enabled_size = size(down_enabled{layer_index}, 2);
			down_enabled{layer_index}(down_enabled_size - filter_size + 1 : down_enabled_size) = 0;
			% add up the transposed convolution
                        down_transposed_convolution_sum{layer_index} += down_transposed_convolution{layer_index, feature_index};
                        % apply the ReLu to the transposed convolution
                        down_output{layer_index, feature_index} = relu(down_transposed_convolution{layer_index, feature_index});
                        % add up the ReLu layer
                        down_output_sum{layer_index} += down_output{layer_index, feature_index};
                endfor
		last_down_enabled = down_enabled{layer_index};
                % the current output will be next layer's input
                current_input = down_output_sum{layer_index};
        endfor

