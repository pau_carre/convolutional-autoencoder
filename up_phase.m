% Up phase
function [up_convolution, up_output, up_pooled, up_input, pooled_indexes] = up_phase(up_input, features, feature_w, filter, bias, pool_size)
	layers = size(filter, 2);
        % from the first to the last layer do...
        for layer_index = 1:layers
                % from the first to the last feature do...
                for feature_index = 1:features{layer_index}
                        % up convolutions initialized to zero
                        up_convolution{layer_index, feature_index} = zeros(1, size(up_input{layer_index, feature_index}, 2) - size(filter{layer_index, feature_index}, 2) + 1 );
                        % for each input (previous layer) convolve using the current feature filter
                        % and add it up to generate the output
                        for input_index = 1:size(up_input, 2)
                                up_convolution{layer_index, feature_index} += convolution(
                                                                                        filter{layer_index, feature_index},
                                                                                        bias{layer_index, feature_index},
                                                                                        up_input{layer_index, feature_index}
                                                                                ); 
                        endfor
                        % apply the ReLu to the final output
                        up_output{layer_index, feature_index} = relu(up_convolution{layer_index, feature_index});
                        % pool the features
                        [up_pooled{layer_index, feature_index}  pooled_indexes{layer_index, feature_index}] = max_pooling(
                                                                                                                up_output{layer_index}, pool_size{layer_index});
                        % the current output will be the next layer's input
                        up_input{layer_index + 1, feature_index}= up_pooled{layer_index, feature_index};
                endfor
        endfor

