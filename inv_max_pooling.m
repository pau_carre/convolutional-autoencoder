function [inv_max_pooled] =  inv_max_pooling(max_pooled, max_pooled_indexes, pool_size, original_size)
inv_max_pooled_size = size(max_pooled, 2) * pool_size;
inv_max_pooled = zeros(1, original_size);
for max_pooled_index = 1:size(max_pooled, 2)
	base_index = ((max_pooled_index - 1 ) * pool_size) + 1;
	for pool_size_index = 1:pool_size
		current_index =  min(original_size, base_index + pool_size_index - 1);
		inv_max_pooled(1, current_index ) = max_pooled(max_pooled_index);
	endfor
endfor
