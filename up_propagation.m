% Up propagation
function [delta, filter, bias] = up_propagation(down_enabled,up_input, features, up_convolution, pooled_indexes, pool_size, learning_rate, filter, up_pooled, delta, bias)  
        layers = size(filter, 2);
	% from the last layer to the first layer do...
        for layer_index = layers:-1:1
                % initialize the propagated delta to zero
                next_delta = zeros(1, size(up_input{layer_index, 1}, 2));
                % from the first feature to the last one do...
                for feature_index = 1:features{layer_index}
                        % get the original size of the layer as the subsampling
                        % might have skipped residual units
                        original_size = size(up_convolution{layer_index, feature_index}, 2);
                        % upsample data to invert the subsampling pass
                        [ current_delta ] = inv_max_pooling(delta, pooled_indexes{layer_index, feature_index},  pool_size{layer_index}, original_size);
                        % the error derivative is the convolution of the current delta and the input
                        error_diff = convolution(current_delta, 0, up_input{layer_index, feature_index});
                        % update the filter and the bias with the current error derivative
                        filter{layer_index, feature_index} -= learning_rate .* error_diff;
                        bias{layer_index, feature_index} -= learning_rate * mean(current_delta);
                        % add up the deltas to construct the propagated delta
                        if layer_index > 1
                                next_delta += transposed_convolution(
                                                filter{layer_index, feature_index}, 0, current_delta
                                                ) .*   relu_diff(up_pooled{layer_index - 1, feature_index});
                        endif
                endfor
                % update the current delta 
                % with the propagated one
                delta = next_delta;
        endfor


