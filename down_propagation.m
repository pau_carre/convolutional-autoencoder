% Down propagation
function [delta, filter, bias] = down_propagation(down_enabled, up_pooled, features, down_inv_pooled, delta, filter, bias, pool_size, pooled_indexes, learning_rate) 
        layers = size(filter, 2); 
	% from the first layer to the last one
        for layer_index = 1:layers
                % initilaize to zeros the delta
                next_delta = zeros(1, size(up_pooled{layer_index, 1}, 2));
                % from the first feature to the last one do...
                for feature_index = 1:features{layer_index}
                        % the error derivative is the convolution of the delta and the input 
                        size_down_inv_pooled = size(down_inv_pooled{layer_index, feature_index}, 2);
			new_down_inv_pooled = down_inv_pooled{layer_index, feature_index}(1: size_down_inv_pooled - 0);
			enabled_delta = delta .* down_enabled{layer_index};
			error_diff = convolution(new_down_inv_pooled, 0.0,  enabled_delta );
                        % update the filter and the bias with the error derivative
                        filter{layer_index, feature_index} -= learning_rate .* error_diff;
                        bias{layer_index, feature_index} -= learning_rate * mean(enabled_delta);
                        % the delta for the next layer is the delta convolced with the filter
                        % multiplied by the derivative of the "raw input"
                        convolved_delta = convolution(
                                                filter{layer_index, feature_index}, 0, enabled_delta
                                                ) .* relu_diff(new_down_inv_pooled);
			% the next layer delta for a given feature is the addition of all 
                        % the propagated input deltas
                        next_delta += diff_max_pooling(convolved_delta, pool_size{layer_index}, pooled_indexes{layer_index, feature_index});
		endfor
                % the current delta is the delta 
                % propagated
                delta = next_delta;
        endfor

