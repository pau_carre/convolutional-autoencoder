function [max_pooled] =  diff_max_pooling(data, pool_size, max_pooled_indexes)
max_pooled_size = idivide(size(data, 2), pool_size, 'ceil');
max_pooled = zeros(1, max_pooled_size);
for max_pooled_index = 1:max_pooled_size
	max_index = max_pooled_indexes(1, max_pooled_index);
	max_pooled(1, max_pooled_index) = data(1, ( ( max_pooled_index - 1 ) * pool_size ) + max_index);
endfor
