function [transposed_enabled] = transposed_convolution_enabled(filter_size, inv_max_pool_enabled)
transposed_enabled_size = size(inv_max_pool_enabled, 2) + filter_size - 1;
transposed_enabled = ones(1, transposed_enabled_size);
for enabled_index = 1:size(inv_max_pool_enabled, 2)
        for filter_index = 1: filter_size
		if( transposed_enabled(1, enabled_index + filter_index - 1) == 1 && inv_max_pool_enabled(1, enabled_index ) == 0)
                	transposed_enabled(1, enabled_index + filter_index - 1) = 0;
		endif
        endfor
endfor
