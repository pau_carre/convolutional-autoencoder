function [max_pooled, max_pooled_indexes] =  max_pooling(data, pool_size)
max_pooled_size = idivide(size(data, 2), pool_size, 'ceil');
max_pooled = zeros(1, max_pooled_size);
max_pooled_indexes = zeros(1, max_pooled_size);
for max_pooled_index = 1:max_pooled_size 
	first_index  = ( ( max_pooled_index - 1 ) * pool_size ) + 1;
	last_index   = ( ( max_pooled_index     ) * pool_size );
	last_index = min( last_index, size(data, 2) );
	[max_value, max_index] = max(data(1, first_index:last_index));
	max_pooled(1, max_pooled_index) = max_value;
        max_pooled_indexes(1, max_pooled_index) = max_index;
endfor
