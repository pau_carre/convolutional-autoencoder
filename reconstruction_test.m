data_size = size(data, 2);
[reconstructed_data, down_enabled] = reconstruct(data, filter, bias, pool_size, features, feature_w);
data_enabled = data .* down_enabled{1};
x_axis = [1:data_size];
reconstruct_plot = plotyy (x_axis, data_enabled, x_axis, reconstructed_data{1});
ylabel (reconstruct_plot(1), 'Actual Data');
ylabel (reconstruct_plot(2), 'Reconstructed Data');
xlabel ('Time');
maximum_y = max(max(data_enabled), max(data)) + 20; 
ylim(reconstruct_plot(1), [0 maximum_y])
ylim(reconstruct_plot(2), [0 maximum_y])
