% data: array of a one dimensional signal to reconstruct
% learning_rate: learning rate for backpropagation
% max_epoch: epochs in the training
% filter: bidimensional cell array. The first index is the feature index
%         and the second index is the layer index. The value is an array that
%         describes thenfilter forn the given feature and layer.
%         All the filters in the same layer must have the same size.
% bias: bidimensional cell array. The first index is the feature index
%         and the second index is the layer index. The value is a number that
%         describes the bias for the given feature and layer.
%         The bias must be just a single floating point number (this is a
%         restriction given by the backpropagation equation in convolutions)
% pool_size: unidimensional cell array where the index is the layer index.
%         The value is a number that describes the elements that are taken
%         into account in the max pooling groups.
% features: unidimensional cell array where the index is the layer index.
%         The value is a number that describes the number of features in the
%         layer.
% features_w: unidimensional cell array where the index is the layer index.
%         The value is an array that gives a global weight for the feature.
%         The feature index in the layer is the index in the array.
%         (for this version it is not used in the training)
%
function [e, filter, bias] = convolutional_autoencoder(data, learning_rate, max_epoch, filter, bias, pool_size, features, feature_w)
for max_epoch_index = 1:max_epoch

	layers = size(filter, 2);

	for feature_index = 1:features{1}
		up_input{1, feature_index} = data * feature_w{1}(feature_index, 1);
	endfor

	% propagate the data upwards (to the last layer)
	[up_convolution, up_output, up_pooled, up_input, pooled_indexes] = up_phase(up_input, features, feature_w, filter, bias, pool_size);

	% the current input is the last output which is
	% the "last iteration input"
	current_input = up_input{layers + 1};

	% propagate the data downwards (back to the input)
	[down_enabled, down_output_sum, down_transposed_convolution_sum, down_inv_pooled, down_transposed_convolution, down_output] = down_phase(up_input,
		up_output, current_input, pooled_indexes, pool_size, filter, bias, features, ones(1, size(current_input, 2)));

	% Reconstruction error
	mse = mean( ( ( data .* down_enabled{1} ) - (down_output_sum{1} .* down_enabled{1}) ).^2);
	e = sqrt(mse);

	% Last layer error
	delta = ( ( down_output_sum{1} .* down_enabled{1} ) - ( data .* down_enabled{1} ) ) .* ( relu_diff(down_transposed_convolution_sum{1}) .* down_enabled{1} );

	% propagate errors from the input to the output layer
        [delta, filter, bias] = down_propagation(down_enabled, up_pooled, features, down_inv_pooled, delta, filter, bias, pool_size, pooled_indexes, learning_rate);
	% propagate errors from the output layer back to the input
	[delta, filter, bias] = up_propagation(down_enabled, up_input, features, up_convolution, pooled_indexes, pool_size, learning_rate, filter, up_pooled, delta, bias);

	[filter] = normalize_filter(filter, features);
endfor
