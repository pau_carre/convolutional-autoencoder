% Down propagation
function [filter] = normalize_filter(filter, features) 
        layers = size(filter, 2); 
	% from the first layer to the last one
        for layer_index = 1:layers
                % from the first feature to the last one do...
                for feature_index = 1:features{layer_index}
                        norm  =  sqrt(sum(filter{layer_index, feature_index}.^2));
                	filter{layer_index, feature_index} =   filter{layer_index, feature_index} ./ norm;
		endfor
        endfor

