function [e, filter, bias] = harmonic_test(data, max_iter, epoch, filter, bias)
topology_init_filter;
e = 1000000000;
learning_rate = 0.0000000000000000001;
for iter = 1 : max_iter
	[e_tmp, filter_tmp, bias_tmp] = convolutional_autoencoder(data, learning_rate, epoch, filter, bias, pool_size, features, feature_w);
	if !isnan(bias_tmp{1, 1}) && !isinf(abs(bias_tmp{1, 1})) && e_tmp < e
		filter = filter_tmp;
		bias = bias_tmp;
		e = e_tmp;
		learning_rate = learning_rate * 10.0;
	else
		learning_rate = learning_rate / 10.0;
	endif
	disp ("Current MSE:"), disp (e)
	fflush(stdout);
endfor
