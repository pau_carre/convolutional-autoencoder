function  [convolved_data] = convolution(filter, bias, data)
convolved_data_size = size(data, 2) - size(filter, 2) + 1;
convolved_data = zeros(1, convolved_data_size);
for convolved_data_index = 1:size(convolved_data, 2)
        for filter_index = 1:size(filter, 2)
                convolved_data(1, convolved_data_index) += data(1, convolved_data_index + filter_index - 1) * filter(1, filter_index);
        endfor
	convolved_data(1, convolved_data_index) += bias;
endfor
