function [result] = relu_diff(data)
	result = data >= 0;
