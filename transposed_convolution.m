function [transposed_data] = transposed_convolution(filter, bias, data)
transposed_data_size = size(data, 2) + size(filter, 2) - 1;
transposed_data = zeros(1, transposed_data_size);
for data_index = 1:size(data, 2)
        for filter_index = 1:size(filter, 2)
                transposed_data(1, data_index + filter_index - 1) += data(1, data_index) * filter(1, filter_index);
        endfor
endfor
for transposed_data_index = 1:size(transposed_data, 2)
        transposed_data(1, transposed_data_index) += bias;
endfor
% trim out the part of the transposed convolution
% that is not fully generated from data
% filter_size = size(filter, 2);
% transposed_data = transposed_data(1, filter_size - 1 : transposed_data_size - filter_size + 1 );
