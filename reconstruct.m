function [down_output_sum, down_enabled] = reconstruct(data, filter, bias, pool_size, features, feature_w)
layers = size(filter, 2);

for feature_index = 1:features{1}	
	up_input{1, feature_index} = data * feature_w{1}(feature_index, 1);
endfor	

[up_convolution, up_output, up_pooled, up_input, pooled_indexes] = up_phase(up_input, features, feature_w, filter, bias, pool_size);

% the current input is the last output which is 
% the "last iteration input"
current_input = up_input{layers + 1}; 

[down_enabled, down_output_sum, down_transposed_convolution_sum, down_inv_pooled, down_transposed_convolution, down_output] = down_phase(up_input, 
	up_output, current_input, pooled_indexes, pool_size, filter, bias, features, ones(1, size(current_input, 2)));
down_output_sum{1} = down_output_sum{1} .* down_enabled{1};

